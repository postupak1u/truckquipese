    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="index.php"><i class="fa fa-home"></i>Ma boutique</a>
                <div class="nav-collapse collapse">

                    <form class="navbar-form form-search pull-right">
                        <input id="Search" name="Search" type="text" placeholder="Recherche..." class="input-medium search-query">
                        <button type="submit" class="btn">Rechercher</button>
                        <?php
                        if(isset($_SESSION['connecte'])) {
                            echo "<a href='#' id='logOut' onclick='logOut();'>Se déconnecter</a>";
                        }
                        ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function logOut() {
            window.location = "fonctions/logOut.php";
            return false;
        }
    </script>