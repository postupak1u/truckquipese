<footer id="footer" class="vspace20">
    <div class="container">
        <div class="row">
            <div class="span4">
                <h4>Services et Garanties</h4>
                <ul class="nav nav-stacked">
                    <li><a href="#">Garanties et assurances</a>
                    <li><a href="#">Coûts d'envoi</a>
                    <li><a href="#">Conditions d'envoi</a>
                    <li><a href="#">Service Après-Vente</a>
                    <li><a href="#">Engagements</a>
                </ul>
            </div>

            <div class="span4">
                <h4>Contacts</h4>
                <p><i class="icon-map-marker"></i>&nbsp;IUT Nancy Charlemagne, 54000 Nancy</p>
                <p><i class="icon-phone"></i>&nbsp;03 01 02 03 04</p>
                <p><i class="icon-envelope"></i>&nbsp;boutique@enligne.fr</p>
                <p><i class="icon-globe"></i>&nbsp;<a href="http://www.theuselessweb.com/">http://www.boutique-de-test.toto/</a></p>
            </div>

            <div class="span4">
                <h4>Newsletter</h4>
                <p>Inscrivez-vous à la newsletter !</p>
                <form class="form-newsletter">
                    <div class="input-append">
                        <input type="email" class="span2" placeholder="Votre email">
                        <button type="submit" class="btn">S'inscrire</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="span6">
                <p>&copy; Copyright 2015 - Tous droits réservés - Site web crée par <strong>Randy HOCINE</strong></p>
            </div>
        </div>
    </div>
</footer>