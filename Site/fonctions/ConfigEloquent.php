<?php

namespace fonctions;
use Illuminate\Database\Capsule\Manager as DB;

class ConfigEloquent{


    public static function initBDD($filename){
        $db=new DB();
        $db->addConnection(parse_ini_file($filename));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}
?>