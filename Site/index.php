<?php
/*
 * Require autoload
 */
require_once('vendor/autoload.php');
include("incs/header.php");
include("incs/navbar.php");
/*
 * Use zone
 */
use fonctions\ConfigEloquent;
use model\Item;
use model\Piece;
use model\Type;
use model\User;
/*
 * init base de données
 */
ConfigEloquent::initBDD("dbInfos.ini");

$listCouleur=Item::distinct()->select('couleur')->get();

$listItem= Item::all();
$listPieces=Piece::orderBy('nom')->get();
if(isset($_GET['Search'])){
    $listItem=Item::where('nom','LIKE','%'.$_GET['Search'].'%')->get();
}
if(isset($_GET['idPiece'])){
    $listItem=Item::where('piece_id', '=', $_GET['idPiece'])->get();
}
if(isset($_GET['optionsRadios'])){
    if($_GET['optionsRadios']=='option2') {
        $listItem = Item::orderBy('prix')->get();
    }
}

$listTypes=Type::orderBy('type')->get();
if(isset($_SESSION['panier']))
$listPanier=$_SESSION['panier'];

if(isset($_GET['couleur'])) {
    $listItem = Item::where('couleur', '=', $_GET['couleur'])->get();
}
if(isset($_GET['type_id'])){
    $strParam = $QUERY_STRING;
    while($strParam)
    {
        $tabParams[ereg_replace("^([^=]+)=[^&]+&?.*$","\\1",$strParam)] = ereg_replace("^[^=]+=([^&]+)&.*$","\\1",$strParam);
        $strParam = ereg_replace("(^[^=]+=[^&]+&?)(.*$)","\\2",$strParam);
        $i--;
    }
    $i=0;
    while( list($var, $val) = each($tabParams) )
        echo $i++.": $var=>$val<br />\n";

    $listItem = Item::where('type_id', '=', $_GET['type_id'])->get();

}
/*
 * GESTION CONNEXION
 */
if(isset($_POST['email']) && isset($_POST['pass'])){
    $user=User::where('email','=',$_POST['email']);
    if($user->count()==0)
        echo'Connexion impossible';
    else{
        $boolean=false;
        foreach($user as $u)
            $boolean=password_verify($_POST['pass'], $u->password);
        if($boolean)
            echo'Erreur de connexion';
        else{
            $_SESSION['connecte']=true;
            $_SESSION['email']=$_POST['email'];
            echo('<script>document.location.href="index.php";</script>');
        }

    }
}
else {
    if (isset($_POST['email'])) {
        echo 'Erreur de connexion';}
}
?>
	<div class="container">
		<div class="row">
			<div class="span3">
				<div class="well">

					<div class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="icon-shopping-cart"></i>
							Panier
							<b class="caret"></b></a>
						</a>
						<div class="dropdown-menu well" role="menu" aria-labelledby="dLabel">
                            <?php
                            echo'Panier non géré, coming soon ;-)';
                            ?>
						</div>
					</div>

				</div>

				<div class="well">

						<h4>Pieces</h4>
                    <ul class="nav nav-list">
                        <?php

                        foreach($listPieces as $piece){
                            echo('<li class="item"><a href="index.php?idPiece='.$piece->id.'">'.$piece->nom.'</a></li>');
                        }

                        ?>
					</ul>
				</div>

				<div class="well">
					<h4>Type</h4>
					<form method="get">
                        <?php
                        foreach($listTypes as $type){
                            echo('
                            <label class="checkbox">
                                <input type="checkbox" name ="type_id" value="'.$type->id.'">'.$type->type.'
                            </label>


					    ');}
                            ?>
                        <button class="btn btn-primary pull-right" type="submit">Filtrer</button>
					</form>
				</div>
                <div class="well">
                    <h4>Couleurs</h4>
                    <form method="get">
                        <?php
                        foreach($listCouleur as $c){
                           echo('
                            <label class="checkbox">
                                <input type="checkbox" name ="couleur" value="'.$c->couleur.'">'.$c->couleur.'
                            </label>')



					    ;}
                        ?>
                        <button class="btn btn-primary pull-right" type="submit">Filtrer</button>
                    </form>
                </div>

				<div class="well">
					<h4>Trier par</h4>
					<form>
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                            Rien
                        </label>
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                            Prix croissant
                        </label>
                        <button class="btn btn-primary pull-right" type="submit">Trier</button>
                    </form>
				</div>
                <?php
                if(!isset($_SESSION['connecte'])) {
                    ?>
                    <div class="well">
                        <form class="form login-form" method="post">
                            <h2>Connexion</h2>

                            <div>
                                <label>Email</label>
                                <input id="email" name="email" type="text"/>

                                <label>Mot de passe</label>
                                <input id="pass" name="pass" type="password"/>
                                <!--
                                <label class="checkbox inline">
                                    <input type="checkbox" id="RememberMe" value="option1">Se souvenir de moi
                                </label>
                                -->
                                <br/><br/>

                                <button type="submit" class="btn btn-success">Connexion</button>
                            </div>
                            <br/>
                            <a href="inscription.php">S'inscrire</a><!--&nbsp;&#124;&nbsp;<a href="#">Mot de passe oublié ?</a>-->
                        </form>
                    </div>
                <?php
                }
                else{
                    echo ('<div class="success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Vous êtes connecté</strong>
                    </div>');
                }
                ?>
			</div>

			<div class="span9">
                <?php
                    if(!isset($_GET['idPiece']) && !isset($_GET['couleur']) && !isset($_GET['optionsRadios']) &&!isset($_GET['type_id']) && !isset($_GET['Search'])) {
                        ?>
                        <div class="hero-unit">
                            <p>Articles mis en avant cette semaine</p>
                            <div class="slideshow">
                                <ul>
                                    <li><a href="detailProduit.php?id=3"><img src="img/fauteuil.jpg" alt=""/></a></li>
                                    <li><a href="detailProduit.php?id=6"><img src="img/tortue_rouge.jpg" alt=""/></a></li>
                                    <li><a href="detailProduit.php?id=4"><img src="img/vache_blanche.jpg" alt=""/></a></li>
                                    <li><a href="detailProduit.php?id=8"><img src="img/canard_gonflable.jpg" alt=""/></a></li>
                                </ul>
                            </div>

                        </div>
                    <?php
                    }
                ?>
				<script type="text/javascript" src="js/ajax.js">
				</script>
				
				<script type="text/javascript">
					   $(function(){
					      setInterval(function(){
					         $(".slideshow ul").animate({marginLeft:-500},800,function(){
					            $(this).css({marginLeft:0}).find("li:last").after($(this).find("li:first"));
					         })}, 4000);
					   });
				</script>
                <?php
                if(!isset($_GET['idPiece']) && !isset($_GET['couleur']) && !isset($_GET['optionsRadios']) && !isset($_GET['type_id'])&& !isset($_GET['Search'])) {
                    ?>
				<!--Articles les mieux notés-->
				<ul class="thumbnails">
					<li class="span9">
						<div class="thumbnail">
							<div class="caption">
								<h4 id="topnote">Nos articles les mieux notés</h4>
							</div>
						</div>
					</li>
                </ul>
					<?php

                        echo('<ul class="thumbnails">');
                        $topList = Item::orderBy('moyenne')->take(3)->get();
                        foreach ($topList as $itemActuel) {
                            echo('
							<li class="span3">
								<div class="thumbnail">
									<img src="img/' . $itemActuel->photo . '" alt="">
									<div class="caption">
										<h4>' . $itemActuel->nom . '</h4>
										<p>Prix : ' . $itemActuel->prix . ' € </p>
										<a class="btn btn-primary" href="detailProduit.php?id=' . $itemActuel->id . '">Aperçu</a>
										<a class="btn btn-success" href="#">Ajouter au panier</a>
									</div>
								</div>
							</li>
						');
                        }
                    echo' </ul>';
                    }
                    ?>


				<!--Articles communs-->

                <ul class="thumbnails">
					<li class="span9">
						<div class="thumbnail">
							<div class="caption">
								<h4 id="topnote">Liste de nos articles</h4>
							</div>
						</div>
					</li>
                </ul>
                <?php
                echo('<ul class="thumbnails">');
                foreach($listItem as $itemActuel){

                echo('
					<li class="span3">
						<div class="thumbnail">
							<img src="img/'.$itemActuel->photo.'" alt="">
							<div class="caption">
								<h4>'.$itemActuel->nom.'</h4>
								<p>Prix : '.$itemActuel->prix.' € </p>
								<a class="btn btn-primary" href="detailProduit.php?id='.$itemActuel->id.'">Aperçu</a>
								<a class="btn btn-success" href="#">Ajouter au panier</a>
							</div>
						</div>
					</li>
				');
                }
                echo('</ul>');?>

                <!-- PAGINATION -->
                <!--
				<div class="pagination">
					<ul>
						<li class"disabled"><span>Préc.</span></li>
						<li class"disabled"><span>1</span></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li><a href="#">Suiv.</a></li>
					</ul>
				</div>-->
			</div>
		</div>
	</div>

<?php include("incs/footer.php");?>

	<script src="js/jquery-1.10.0.min.js"></script>
	<script src="js/bootstrap/js/bootstrap.min.js"></script>
	<script src="js/holder.js"></script>
	<script src="js/script.js"></script>
</body>
</html>