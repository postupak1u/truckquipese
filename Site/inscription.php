<?php


?>



<?php include("incs/header.php");
include("incs/navbar.php");?>

<?php
require_once('vendor/autoload.php');
use fonctions\ConfigEloquent;
use model\User;
ConfigEloquent::initBDD("dbInfos.ini");

if(isset($_POST['email']) && isset($_POST['pass']) && $_POST['email']!='' && $_POST['pass']!=''){
    $user=User::where('email','=',$_POST['email']);
    if($user->count()>0)
        echo'Erreur d\'inscription';
    else{
        echo'k';
        $user=new User;
        $user->email=$_POST['email'];
        $user->password=password_hash($_POST['pass'], PASSWORD_BCRYPT);
        $user->nom=$_POST['nom'];
        $user->prenom=$_POST['prenom'];
        $user->adresse=$_POST['adress'];
        $user->save();
        echo('ok');
    }
}
else
    echo'entrez les infos';

?>
<div class="container">
    <div class="row">
        <div class="span1"></div>
        <div class="span7">
            <div class="well">
                <form action="inscription.php" method="post">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="name">Nom:</label>
                            <input type="text" name="nom" class="form-control" id="name" placeholder="Entrer votre nom">
                        </div>
                        <div class="form-group">
                            <label for="prename">Prenom:</label>
                            <input type="text" name="prenom" class="form-control" id="prename" placeholder="Entrer votre prenom">
                        </div>
                        <label for="email">Email:</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="pass">Password:</label>
                        <input type="password" name ="pass" class="form-control" id="pass" placeholder="Enter password">
                    </div>
                    <div class="form-group">
                        <label for="adress">Adresse:</label>
                        <input type="text" name ="adress" class="form-control" id="adress" placeholder="Entrer votre adresse">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>

                </form>
            </div>
        </div>
        <div class="span4"></div>

    </div>
</div>

<?php include("incs/footer.php");?>

<script src="js/jquery-1.10.0.min.js"></script>
<script src="js/bootstrap/js/bootstrap.min.js"></script>
<script src="js/holder.js"></script>
<script src="js/script.js"></script>
</body>
</html>