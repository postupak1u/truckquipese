<?php
/**
 * Created by PhpStorm.
 * User: Shikate
 * Date: 05/03/2015
 * Time: 15:51
 */

namespace model;


class Type extends \Illuminate\Database\Eloquent\Model{

    protected  $table='ccd_types';
    protected  $primarykey='id';
    public $timestamps='false';

    public function type(){
        return $this->hasMany('model\Meuble','id');
    }

}