<?php
/**
 * Created by PhpStorm.
 * User: Shikate
 * Date: 05/03/2015
 * Time: 15:46
 */

namespace model;


class Piece extends \Illuminate\Database\Eloquent\Model{

    protected  $table='ccd_pieces';
    protected  $primarykey='id';
    public $timestamps='false';

    //??????
    public function piece(){
        return $this->hasMany('model\Item','piece_id');
    }

    /* public function commentaires(){
         return $this->hasMany('blogapp\model\Commentaire','id');
     }*/

}