<?php
/**
 * Created by PhpStorm.
 * User: Shikate
 * Date: 05/03/2015
 * Time: 21:20
 */

namespace model;


class User extends \Illuminate\Database\Eloquent\Model{

    protected  $table='ccd_users';
    protected  $primarykey='id';
    public $timestamps='false';

    public function type(){
        return $this->hasMany('model\Meuble','id');
    }
} 