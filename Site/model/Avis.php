<?php
/**
 * Created by PhpStorm.
 * User: Shikate
 * Date: 07/03/2015
 * Time: 01:27
 */

namespace model;


class Avis extends \Illuminate\Database\Eloquent\Model{
    protected  $table='ccd_avis';
    protected  $primarykey='id';
    public $timestamps='false';

    public function article(){
        return $this->belongsTo('model\Item','idItem');
    }

} 