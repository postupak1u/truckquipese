<?php
/**
 * Created by PhpStorm.
 * User: Shikate
 * Date: 05/03/2015
 * Time: 15:45
 */

namespace model;


class Item extends \Illuminate\Database\Eloquent\Model{

    protected  $table='ccd_items';
    protected  $primarykey='id';
    public $timestamps='false';

    public function piece(){
        return $this->belongsTo('model\Piece','piece_id');
    }

    public function type(){
        return $this->belongsTo('model\Type','type_id');
    }

   /* public function commentaires(){
        return $this->hasMany('blogapp\model\Commentaire','id');
    }*/

}