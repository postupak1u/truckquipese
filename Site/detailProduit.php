<?php
require_once('vendor/autoload.php');
use fonctions\ConfigEloquent;
use model\Item;
use model\Avis;
ConfigEloquent::initBDD("dbInfos.ini");

/*
 * Initialisation bdd
 */
if(isset($_GET['id'])){
    $id_produit=$_GET['id'];
    $item=Item::find($id_produit);
    $chemin_image_produit=$item->photo;

    $nom_produit=$item->nom;
    $desc_produit=$item->description;
    $prix_produit=$item->prix;
    $couleur_produit=$item->couleur;
    $nbAvisProduit=getNombreAvis($id_produit);
    $avis_produit=getAvis($id_produit);


    /*
     * ZONE D'INCLUDE
     */
    include("incs/header.php");
    include("incs/navbar.php");
?>


<div class="container">
    <div class="row">
        <div class="span3">
            <div class="well">

                <div class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="icon-shopping-cart"></i> Panier
                        <b class="caret"></b></a>
                    </a>
                </div>

            </div>

        </div>

        <div class="span9">
            <div class="row">
                <div class="span5">
                    <div id="items-carousel" class="carousel slide mbottom0">
                        <div class="carousel-inner">
                            <div class="active item">
                                <img class="media-object" src="img/<?php echo($chemin_image_produit) ?>" alt="" />
                            </div>
                        </div>
                </div>
                </div>

                <div class="span4">
                    <h4>Nom : <?php echo ($nom_produit) ?></h4>
                    <h5>ID : <?php echo($id_produit)?></h5>
                    <p>Description : <?php echo($desc_produit) ?></p>
                    <h4>Prix : <?php echo($prix_produit)." € "?></h4>
                    <h5>Couleurs disponibles : </h5>
                    <form>

                        <select id="color" name="color">
                            <?php
                            $listeCouleurs=Item::where("nom","=",$nom_produit)->get();

                            ?>
                            <option>Choisir Couleur</option>
                            <?php
                            foreach($listeCouleurs as $c){
                                echo('<option>'.$c->couleur.'</option>');
                            }
                            ?>
                        </select>

                        <label>
                            <input type="text" id="quantity" name="quantity" value="1" class="span1" />&nbsp;Quantité
                        </label>
                        <button class="btn btn-primary">Ajouter au panier</button>
                    </form>
                    <a href="#">Ajouter à la liste de souhait</a>

                </div>
            </div>

            <div class="row">
                <div class="span9">
                    <ul class="nav nav-tabs" id="tabs">
                        <li><a href="#description">Description</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="description">
                            <p> <?php echo $desc_produit ?></p>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" id="tabs">
                        <li><a href="#reviews"><span class="badge badge-inverse"><?php echo($nbAvisProduit)?></span> Avis</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="reviews">
                            <?php
                                if(isset($_SESSION['connecte']) && $_SESSION['connecte']){
                                    echo getFormulaireAvis();

                                }
                            else{
                                echo 'connectez vous pour poster un commentaire';
                            }
                            echo $avis_produit ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr />

<?php
/*
 * ZONE D'INCLUDE
 */
include("incs/footer.php");

}else{
        echo('erreur');
}?>

<?php

function getNombreAvis($id){

    $nbAvis=Avis::where('id','=',$id)->count();
    return $nbAvis;
    //return 0;
}

function getAvis($id){
    if(getNombreAvis($id)>0){
        $string="";
        $listAvis=Avis::where('idItem','=',$id)->get();
        var_dump($listAvis);
        if($listAvis->count()>0) {
            foreach ($listAvis as $avis) {
                $string.= '<p>' . $avis->libelle . '</p><hr>';

            }
        }
    }
    else
        return '<p> Aucun avis à afficher</p>';
}

function getFormulaireAvis(){

}
?>
<!--
Zone de script js
-->
<script src="./js/jquery-1.10.0.min.js"></script>
<script src="./js/bootstrap/js/bootstrap.min.js"></script>
<script src="./js/holder.js"></script>
<script src="./js/script.js"></script>
</body>
</html>