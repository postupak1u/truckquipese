-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 05 Mars 2015 à 11:12
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `db567962232`
--

-- --------------------------------------------------------

--
-- Structure de la table `ccd_commentaires`
--

CREATE TABLE IF NOT EXISTS `ccd_commentaires` (
  `idCommentaire` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) DEFAULT NULL,
  `idItem` int(11) DEFAULT NULL,
  `commentaire` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idCommentaire`),
  KEY `idUser` (`idUser`),
  KEY `idItem` (`idItem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_items`
--

CREATE TABLE IF NOT EXISTS `ccd_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `piece_id` int(11) NOT NULL,
  `photo` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `couleur` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `prix` float NOT NULL,
  `type_id` int(11) NOT NULL,
  `promo` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Contenu de la table `ccd_items`
--

INSERT INTO `ccd_items` (`id`, `nom`, `description`, `piece_id`, `photo`, `couleur`, `prix`, `type_id`, `promo`) VALUES
(1, 'Fauteuil gonflable ', '', 1, 'fauteuil_gonflable.jpg', 'Mauve', 150, 4, 0.3),
(2, 'Sofa Gonflable', '', 1, 'sofa_gonflable.jpg', 'Orange', 175.99, 4, 0),
(3, 'Fauteuil en carton', '', 1, 'fauteuil.jpg', 'Maron', 15.95, 1, 0),
(4, 'Vache', '', 2, 'vache_blanche.jpg', 'Blanche', 9.99, 1, 0),
(5, 'Fauteuil en palettes', '', 2, 'meuble_palette.jpg', 'Marron', 230.9, 2, 0),
(6, 'Tortue', '', 3, 'tortue_rouge.jpg', 'Rouge', 25.85, 1, 0),
(7, 'Tortue', '', 3, 'tortue_brun.jpg', 'Brun', 25.85, 1, 0),
(8, 'Canard Gonflable', '', 4, 'canard_gonflable.jpg', 'Jaune', 4.99, 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ccd_notes`
--

CREATE TABLE IF NOT EXISTS `ccd_notes` (
  `idNote` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) DEFAULT NULL,
  `idItem` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNote`),
  KEY `idItem` (`idItem`),
  KEY `idUser` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_pieces`
--

CREATE TABLE IF NOT EXISTS `ccd_pieces` (
  `id` int(11) NOT NULL,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `ccd_pieces`
--

INSERT INTO `ccd_pieces` (`id`, `nom`, `description`) VALUES
(1, 'Salon', 'Là où on se repose'),
(2, 'Salle à manger', 'Là où on mange'),
(3, 'Chambre à coucher', 'Là où on fait dodo'),
(4, 'Salle de bain', 'J''en ressort tout propre');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_types`
--

CREATE TABLE IF NOT EXISTS `ccd_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ccd_types`
--

INSERT INTO `ccd_types` (`id`, `type`) VALUES
(1, 'Carton'),
(2, 'Récupération'),
(3, 'Recyclage'),
(4, 'Gonflable');

-- --------------------------------------------------------

--
-- Structure de la table `ccd_users`
--

CREATE TABLE IF NOT EXISTS `ccd_users` (
  `idUser` int(11) NOT NULL DEFAULT '0',
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `adresse` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ccd_votes`
--

CREATE TABLE IF NOT EXISTS `ccd_votes` (
  `idVote` int(11) NOT NULL DEFAULT '0',
  `idUser` int(11) DEFAULT NULL,
  `idItem` int(11) DEFAULT NULL,
  PRIMARY KEY (`idVote`),
  KEY `idUser` (`idUser`),
  KEY `idItem` (`idItem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `ccd_commentaires`
--
ALTER TABLE `ccd_commentaires`
  ADD CONSTRAINT `ccd_commentaires_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `ccd_users` (`idUser`),
  ADD CONSTRAINT `ccd_commentaires_ibfk_2` FOREIGN KEY (`idItem`) REFERENCES `ccd_items` (`id`);

--
-- Contraintes pour la table `ccd_notes`
--
ALTER TABLE `ccd_notes`
  ADD CONSTRAINT `ccd_notes_ibfk_4` FOREIGN KEY (`idUser`) REFERENCES `ccd_users` (`idUser`),
  ADD CONSTRAINT `ccd_notes_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `ccd_users` (`idUser`),
  ADD CONSTRAINT `ccd_notes_ibfk_2` FOREIGN KEY (`idItem`) REFERENCES `ccd_items` (`id`),
  ADD CONSTRAINT `ccd_notes_ibfk_3` FOREIGN KEY (`idItem`) REFERENCES `ccd_items` (`id`);

--
-- Contraintes pour la table `ccd_votes`
--
ALTER TABLE `ccd_votes`
  ADD CONSTRAINT `ccd_votes_ibfk_1` FOREIGN KEY (`idUser`) REFERENCES `ccd_users` (`idUser`),
  ADD CONSTRAINT `ccd_votes_ibfk_2` FOREIGN KEY (`idItem`) REFERENCES `ccd_items` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
